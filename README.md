
```sh
curl -LO 'https://raw.githubusercontent.com/khonsaloh/linux-desktop-deploy/master/setup'
sh ./setup
```

to resolve packages not found in other distros:
example for `apt-get`
```sh
grep '^,' paquetes.csv| cut -d',' -f2 |xargs apt-get install -s >/dev/null
```
